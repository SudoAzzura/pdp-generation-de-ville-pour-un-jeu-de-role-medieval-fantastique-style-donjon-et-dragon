package fr.u_bordeaux.ville.display;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;
import fr.u_bordeaux.ville.algorithms.Algorithm;
import fr.u_bordeaux.ville.algorithms.VoronoiAlgorithmStreet;
import fr.u_bordeaux.ville.algorithms.VoronoiAlgorithmDistrict;
import fr.u_bordeaux.ville.algorithms.PointAlgorithms.PointAlgorithm;
import fr.u_bordeaux.ville.algorithms.PointAlgorithms.PointAlgorithmSquared;
import fr.u_bordeaux.ville.algorithms.PointAlgorithms.PointAlgorithmRandom;
import fr.u_bordeaux.ville.urban_elements.Group;
import fr.u_bordeaux.ville.urban_elements.MainStreet;
import fr.u_bordeaux.ville.urban_elements.Street;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import de.alsclo.voronoi.graph.Point;

public class DisplayWindow extends JFrame implements ActionListener,DisplayWindowInterface,ChangeListener{
    Display d;
    UrbanElement u;
    List<Point> lD;
    List<Point> lDnew;
    List<Point> lS;
    List<Point> lSnew;

    PointAlgorithm paD;
    PointAlgorithm paS;

    Boolean isGenerate = false;

    private JFrame frame;
    private JFrame frameAwt;
    private JSlider widthJS;
    private JSlider heightJS;
    private JSlider nbDistrictJSlider;
    private JSlider nbStreetJSlider;

    private JButton btnGenerate,btnQ,btnS,btnBg,btnB,btnSave;

    private String streetString = "Couleur des Rues";
    private String districtString = "Couleur des Quartiers";
    private String backgroundString = "Couleur du Fond";
    private String buildingString = "Couleur des Bâtiments";


    private String generationSentenceString = "Générer";
    private String saveSentenceString = "Sauvegarder";
    private String choiceColorStentence = "Choisissez une couleur";

    JLabel widthJLabel ;
    JLabel heightJLabel;
    JLabel nbDistrictJLabel;
    JLabel nbStreetJLabel;

    private int width;
    private int height;
    private int nbDistrict;
    private int nbStreet;

    private Color bg,s,q,b;

    private JPanel p;
    public DisplayWindow(int windowWidth, int windowHeight, Color district, Color street,Color background ) {

        this.q = district;
        this.s = street;
        this.bg = background;

      // Définir le frame
        this.frame = new JFrame("City Generator");
        this.p = new JPanel();
        frame.setContentPane(p);
        GridLayout grid = new GridLayout(9, 1);
        p.setLayout(grid);
        grid.setHgap(10);
        grid.setVgap(15);

        // Définir les slider pour la taille de l'image
        this.nbDistrict = 50;
        this.nbStreet = 400;
        height(0,2000,100,500);
        width(0,2000,100,500);
        //this.width = this.widthJS.getValue();
        //this.height = this.heightJS.getValue();
        nbDistrict(0,this.nbDistrict,1,10);
        nbStreet(0,this.nbStreet,10,100);

        //Choisir les couleurs
        colors();
        //Choisir si on sauvegarde l'image
        save();
        
        this.btnGenerate = new JButton(generationSentenceString); 
        this.p.add(this.btnGenerate);
        this.btnGenerate.addActionListener(this);

        frame.pack();
        frame.setSize(windowWidth, windowHeight);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    private void printRGBonBtn(JButton b,String s,Color c) {
      b.setText(s+" : ("+c.getRed()+","+c.getGreen()+","+c.getBlue()+")");
    }

    public void actionPerformed(ActionEvent e) {
      if ( e.getSource() == this.btnS) {
    //Couleur pour les Rues(Street)
        this.s = JColorChooser.showDialog(this,choiceColorStentence,this.s);
        this.btnS.setBackground(getStreetColor());
        this.btnS.setForeground(oppositeColor(getStreetColor()));
        printRGBonBtn(this.btnS,this.streetString,this.s);
        if(isGenerate) {changeFrame();}
      } else if(e.getSource() == this.btnQ) {
        //Couleur pour les Quartiers(District)
        this.q = JColorChooser.showDialog(this,choiceColorStentence,this.q);
        this.btnQ.setBackground(getDistrictColor());
        this.btnQ.setForeground(oppositeColor(getDistrictColor()));
        printRGBonBtn(this.btnQ,this.districtString,this.q);
        if(isGenerate) {changeFrame();}
      } else if(e.getSource() == this.btnBg) {
        //Couleur du Fond
        this.bg = JColorChooser.showDialog(this,choiceColorStentence,this.bg);
        this.btnBg.setBackground(getBackgroundColor());
        this.btnBg.setForeground(oppositeColor(getBackgroundColor()));
        printRGBonBtn(this.btnBg,this.backgroundString,this.bg);
        if(isGenerate) {changeFrame();}
      } else if(e.getSource() == this.btnGenerate) {
          this.isGenerate = true;
          PointAlgorithm pa = new PointAlgorithmRandom(this.nbDistrictJSlider.getValue(), 0, this.widthJS.getValue(), 0, this.heightJS.getValue());
          PointAlgorithm pa2 = new PointAlgorithmRandom(this.nbStreetJSlider.getValue(), 0, this.widthJS.getValue(), 0, this.heightJS.getValue());
          this.paD = pa;
          this.paS = pa2;
          // Carré
          // PointAlgorithm pa2 = new PointAlgorithmSquared(this.nbStreet, this.widthJS.getValue(), this.height);

          List<Point> list = pa.getPoints();
          List<Point> list2 = pa2.getPoints();
          this.lD =list;
          this.lS =list2;
          Algorithm va = new VoronoiAlgorithmDistrict(list, this.widthJS.getValue(), this.heightJS.getValue());
          UrbanElement otter = va.generate(null);
          //this.u = otter;
          Algorithm va2 = new VoronoiAlgorithmStreet(list2, this.widthJS.getValue(), this.heightJS.getValue(), otter);
          
          otter = va2.generate(null);
          this.u = otter;
          //Definition des couleurs
          this.frameAwt = new JFrame();
          //System.out.print(this.widthJS.getValue()+" "+this.heightJS.getValue()+"\n");
          this.d = new DisplayAwt(getFrameAwt(),this.widthJS.getValue(),this.heightJS.getValue(),getBackgroundColor(), getStreetColor(), getDistrictColor());
          //Affichage de la ville
          this.d.drawForWindow(this.lD, this.u);
          this.width = widthJS.getValue();
          this.height = heightJS.getValue();
          //Save img
        } else if(e.getSource() == this.btnSave) {
          try {
            this.d.saveImg();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }

    //Pour actualiser La fenêtre entre chaque changement
    private void changeFrame() {
      //on ferme la fenêtre précédente
      this.frameAwt.dispose();
      //nouvelle fenêtre
      this.frameAwt = new JFrame();
      this.d = new DisplayAwt(getFrameAwt(),this.widthJS.getValue(),this.heightJS.getValue(),getBackgroundColor(), getStreetColor(), getDistrictColor());
      this.d.drawForWindow(this.lD,this.u);
    }
    /*
      Changement d'état des Slider
      Dès que la valeur d'un change tout change 
    */
    @Override
    public void stateChanged(ChangeEvent arg0) {
      int tmpw = this.width;
      int tmph = this.height;
      //System.out.print(tmpw+" "+this.widthJS.getValue());
      this.width = this.widthJS.getValue();
      //System.out.print(this.width+"\n");
      this.height = this.heightJS.getValue();
      this.nbDistrict = this.nbDistrictJSlider.getValue();
      this.nbStreet = this.nbStreetJSlider.getValue();
      this.heightJLabel.setText("Hauteur : "+this.heightJS.getValue());
      this.widthJLabel.setText("Largeur : "+this.widthJS.getValue());
      this.nbDistrictJLabel.setText("Nombre de quartiers : "+this.nbDistrictJSlider.getValue());
      this.nbStreetJLabel.setText("Nombre de rues : "+this.nbStreetJSlider.getValue());
      

      /*
      on ne refait pas l'affichage 
      lorsque l'on change le nb de rue ou de district 
      sinon il faudrait refaire une génération de point
      */
      //System.out.print(isGenerate);
      
      if(isGenerate && (tmph != this.heightJS.getValue() || tmpw != this.widthJS.getValue())) {
        int i = 0;
        
        //TODO
        System.out.println("prout\n"+tmph+" "+tmpw+"\n"+this.heightJS.getValue()+" "+this.widthJS.getValue());
        Iterator<Line> children = this.u.getAllChildrenLines();
        UrbanElement new_u = new Group();
        //Iterator<Street> borders = this.u.getAllBordersStreets();
        //Iterator<Position> pt = u.getPointsNotOnBorder();

        while(children.hasNext()){
          i++;
          Line line = children.next();
          changeLine(line, tmph, tmpw);
          Street s = new MainStreet(line.get_start(), line.get_finish(),line.getStroke());
          try {
            new_u.addChildren(s);
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
        System.out.println("i : "+i);
        i=0;
        // while(pt.hasNext()){
        //   changePosition(pt.next(), tmph, tmpw);
        // }

        this.d = new DisplayAwt(new JFrame(),this.widthJS.getValue(),this.heightJS.getValue(),getBackgroundColor(), getStreetColor(), getDistrictColor());
        changeFrame();
        this.d.drawForWindow(this.lD,new_u); 
      }
          
    }
    // private void changePosition(Position p, int tmph, int tmpw) {
    //   p.change_y((double)tmph, (double)getHeight());
    //   p.change_x((double)tmpw, (double)getWidth());
    // }
    private void changeLine(Line l, int tmph, int tmpw) {
      System.out.println(l);
      l.get_start().change_y((double)tmph, (double)getHeight());
      l.get_start().change_x((double)tmpw, (double)getWidth());
      l.get_finish().change_x((double)tmpw, (double)getWidth());
      l.get_finish().change_y((double)tmph, (double)getHeight());
      //return l;

    }
    //Slider pour la Hauteur
    private void height(int min, int max, int MinorTickSpacing,int MajorTickSpacing) {
      this.heightJS = new JSlider(min, max);
      this.heightJS.setValue(500);
      this.heightJS.setMajorTickSpacing(MajorTickSpacing);
      this.heightJS.setMinorTickSpacing(MinorTickSpacing);
      this.heightJS.setPaintTicks(true);
      this.heightJS.setPaintLabels(true);
      this.heightJS.setName("Hauteur");
      this.heightJS.addChangeListener((ChangeListener)this);
      this.heightJLabel= new JLabel();
      this.heightJLabel.setText("Hauteur : "+this.heightJS.getValue());
      this.p.add(this.heightJLabel);
      this.p.add(this.heightJS);
    }
    //Slider pour la Largeur
    private void width(int min, int max, int MinorTickSpacing,int MajorTickSpacing) {
        this.widthJS = new JSlider(min,max);
        this.widthJS.setValue(500);
        this.widthJS.setMinorTickSpacing(MinorTickSpacing);
        this.widthJS.setMajorTickSpacing(MajorTickSpacing);
        this.widthJS.setPaintTicks(true);
        this.widthJS.setPaintLabels(true);
        this.widthJS.setName("Largeur");
        this.widthJS.addChangeListener((ChangeListener)this);
        this.widthJLabel= new JLabel();
        this.widthJLabel.setText("Largeur : "+this.widthJS.getValue());
        this.p.add(this.widthJLabel);
        this.p.add(this.widthJS);
    }

    //Slider pour le nb de quartier
    private void nbDistrict(int min, int max, int MinorTickSpacing,int MajorTickSpacing) {
      this.nbDistrictJSlider = new JSlider(min, max, max);
      this.nbDistrictJSlider.setMajorTickSpacing(MajorTickSpacing);
      this.nbDistrictJSlider.setMinorTickSpacing(MinorTickSpacing);
      this.nbDistrictJSlider.setPaintTicks(true);
      this.nbDistrictJSlider.setPaintLabels(true);
      this.nbDistrictJSlider.setName("Nombre de quartiers : ");
      this.nbDistrictJSlider.addChangeListener((ChangeListener)this);
      this.nbDistrictJLabel= new JLabel();
      this.nbDistrictJLabel.setText("Nombre de districts : "+this.nbDistrictJSlider.getValue());
      this.p.add(this.nbDistrictJLabel);
      this.p.add(this.nbDistrictJSlider);
    }
    //Slider pour le nb de rue
    private void nbStreet(int min, int max, int MinorTickSpacing,int MajorTickSpacing) {
      this.nbStreetJSlider = new JSlider(min, max, max);
      this.nbStreetJSlider.setMajorTickSpacing(MajorTickSpacing);
      this.nbStreetJSlider.setMinorTickSpacing(MinorTickSpacing);
      this.nbStreetJSlider.setPaintTicks(true);
      this.nbStreetJSlider.setPaintLabels(true);
      this.nbStreetJSlider.setName("Nombre de rues : ");
      this.nbStreetJSlider.addChangeListener((ChangeListener)this);
      this.nbStreetJLabel= new JLabel();
      this.nbStreetJLabel.setText("Nombre de rues : "+this.nbStreetJSlider.getValue());
      this.p.add(this.nbStreetJLabel);
      this.p.add(this.nbStreetJSlider);
    }

    //Boutons pour choisir les différentes couleur
    private void colors(){
      //Couleur Quartier
      this.btnQ = new JButton();
      this.btnQ.setBackground(getDistrictColor());
      this.btnQ.setForeground(oppositeColor(getDistrictColor()));
      this.btnQ.addActionListener(this);
      printRGBonBtn(this.btnQ,this.districtString,this.q);
      this.frame.add(btnQ);
      //Couleur rue
      this.btnS = new JButton(streetString);
      this.btnS.setBackground(getStreetColor());
      this.btnS.setForeground(oppositeColor(getStreetColor()));
      this.btnS.addActionListener(this);
      printRGBonBtn(this.btnS,this.streetString,this.s);
      this.frame.add(btnS);
      //Couleur fond
      this.btnBg = new JButton();
      this.btnBg.setBackground(getBackgroundColor());
      this.btnBg.setForeground(oppositeColor(getBackgroundColor()));
      printRGBonBtn(this.btnBg,this.backgroundString,this.bg);
      this.btnBg.addActionListener(this);
      this.frame.add(btnBg);
      //Building
      // this.btnB = new JButton(backgroundString);
      // this.btnB.setBackground(getBackgroundColor());
      // this.btnB.setForeground(oppositeColor(getBuildingColor()));
      // this.btnB.addActionListener(this);
      // this.frame.add(btnB);
    }

    /*
    Pour le texte des boutons juste esthétique
    On prend la couleur opposée du bouton
    */
    private Color oppositeColor(Color c) {
      int red = (int)Math.abs(c.getRed()-255);
      int green = (int)Math.abs(c.getGreen()-255);
      int blue = (int)Math.abs(c.getBlue()-255);
      Color opColor = new Color(red, green, blue);
      return opColor;
    }

    //Bouton pour Sauvegarder
    private void save() {
      this.btnSave = new JButton();
      this.btnSave.addActionListener(this);
      this.btnSave.setText(saveSentenceString);
      this.frame.add(btnSave);
    }

    @Override
    public Color getBackgroundColor() {
      return this.bg;
    }

    @Override
    public Color getBuildingColor() {
      return this.b;
    }

    @Override
    public Color getStreetColor() {
      return this.s;
    }

    @Override
    public Color getDistrictColor() {
      return this.q;
    }
    
    @Override
    public int getWidth() {
      return this.width;
    }

    @Override
    public int getHeight() {
      return this.height;
    }
    @Override
    public JFrame getFrameAwt() {
      return this.frameAwt;
    }
}
