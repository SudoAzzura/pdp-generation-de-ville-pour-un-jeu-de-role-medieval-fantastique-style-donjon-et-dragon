package fr.u_bordeaux.ville.display;

import fr.u_bordeaux.ville.urban_elements.UrbanElement;
import java.io.IOException;
import java.util.List;

import de.alsclo.voronoi.graph.Point;

import java.awt.*;
public interface Display {
    //could be difficult to do cause u is eather a group or an element (and a group is eatehr composed of groups or element, or both and so on)
    void draw(UrbanElement u);
    void draw(List<Point> l,UrbanElement u);
    void drawForWindow(List<Point> l,UrbanElement u);

    void saveImg() throws IOException;
    void saveImg(String format) throws IOException;
    
}