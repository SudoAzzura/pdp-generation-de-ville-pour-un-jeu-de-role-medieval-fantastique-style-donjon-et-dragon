package fr.u_bordeaux.ville.display;

import fr.u_bordeaux.ville.ColorRGB;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;
import fr.u_bordeaux.ville.urban_elements.Street;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;


import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import de.alsclo.voronoi.graph.Point;

import javax.imageio.ImageIO;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DisplayAwt implements Display{
    private Graphics2D g2d;
    private BufferedImage img;
    private JFrame frame;
    private Color msColor = new Color(251, 116, 101);
    private Color ssColor  = new Color(4, 139, 154);;
    private Color ptColor = new Color(255,255,255);
    private Color bgColor ;
    private int height;
    private int width;
    private int minx;
    private int miny;
    private int maxx;
    private int maxy;

    public DisplayAwt(UrbanElement ue,Color backgroundColor,int height,int width) {
        //this.msColor = mainStreetColor;
        //this.ssColor = secondaryStreetColor;
        this.height = height;
        this.width = width;
        //init de la frame
        javax.swing.JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame = frame;
        //init les bornes de l'img
        //createBorn(ue);
        //init de l'img
        this.img =  new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.g2d  = (Graphics2D) this.img.getGraphics();
        this.bgColor = backgroundColor;
        //g2d.setBackground(backgroundColor);
        g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
        
    }
    
    public DisplayAwt(Color backgroundColor,int height,int width) {
        this.width = width;
        //init de la frame
        javax.swing.JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame = frame;
        //init les bornes de l'img
        //createBorn(ue);
        //init de l'img
        this.img =  new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.g2d  = (Graphics2D) this.img.getGraphics();
        this.bgColor = backgroundColor;
        //g2d.setBackground(backgroundColor);
        g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
        
    }

    public DisplayAwt(Color backgroundColor, Color secondaryStreetcolor, Color mainStreetcolor,int height,int width) {
        this.width = width;
        //init de la frame
        javax.swing.JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.frame = frame;

        this.msColor = mainStreetcolor;
        this.ssColor = secondaryStreetcolor;
        this.bgColor = backgroundColor;

        //init de l'img
        this.img =  new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.g2d  = (Graphics2D) this.img.getGraphics();
        this.bgColor = backgroundColor;

        g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
        
    }
    //permet de rester dans la même frame
    //pour l'affichage en live 
    public DisplayAwt(JFrame frame,int width,int height,Color backgroundColor, Color secondaryStreetcolor, Color mainStreetcolor) {
        this.frame = frame;
        this.width = width;
        this.height= height;
        this.msColor = mainStreetcolor;
        this.ssColor = secondaryStreetcolor;
        this.bgColor = backgroundColor;

        //init de l'img
        this.img =  new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_ARGB);
        this.g2d  = (Graphics2D) this.img.getGraphics();
        this.bgColor = backgroundColor;
        //g2d.setBackground(backgroundColor);
        g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
        
    }
    
    private void displayImg(){
        ImageIcon icon = new ImageIcon(this.img);
		
		this.frame.add(new JLabel(icon));
		this.frame.pack();
		this.frame.setVisible(true);
    }

    @Override
    public void saveImg() throws IOException {
        saveImg("png");
    }

    @Override
    public void saveImg(String format) throws IOException {
        DateFormat f2 = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        //System.out.println("./"+f2.format(date)+"_ville."+format);
        ImageIO.write(this.img, format, new File("./"+"images/"+f2.format(date)+"_ville."+format));
    }

    private void setColorRGB() {
        this.msColor = new Color(251, 116, 101);
        this.ssColor = new Color(4, 139, 154);
        this.ptColor = new Color(255,255,255);
    }

    @Override
    public void drawForWindow(List<Point> l,UrbanElement u) {
    	drawStreets(u,this.ssColor);
        drawBorders(u,this.msColor);
//        drawPointOfInterest(l, this.ptColor);
        displayImg();
    }

    @Override
    public void draw(List<Point> l,UrbanElement u) {
        setColorRGB();
        drawStreets(u,this.ssColor);
        drawBorders(u,this.msColor);
//        drawPointOfInterest(l, this.ptColor);
        displayImg();
    }

    @Override
    public void draw(UrbanElement u) {
        setColorRGB();
        drawStreets(u,this.ssColor);
        drawBorders(u,this.msColor);
//        drawPointOfInterest(u, this.ptColor);
        displayImg();
    }

    private void drawBorders(UrbanElement u, Color color){
        Iterator<Street> borders = u.getAllBordersStreets();
        while(borders.hasNext()){
            drawLine(borders.next().getLine(),color);
        }
    }

    private void drawStreets(UrbanElement u,Color color){
        Iterator<Line> children = u.getAllChildrenLines();
        while(children.hasNext()){
            drawLine(children.next(),color);
        }
    }

    private void drawPointOfInterest(UrbanElement u,Color color) {
        Iterator<Position> children = u.getPointsNotOnBorder();
        while(children.hasNext()){
            drawStar(children.next(), color);
        }
    }

    private void drawPointOfInterest(List<Point> l,Color color) {
        for (int i = 0; i< l.size(); i++) {
			Point p = l.get(i);
            drawStar(p, color);
		}
    }

    private void drawStar(Point p, Color color) {
        Graphics2D g = get_g2d();
        ///java.lang.String char = "*";
        g.setColor(color);
        g.drawString("*", (int)p.x, (int)p.y);
    }

    private void drawStar(Position p, Color color) {
        Graphics2D g = get_g2d();
        ///java.lang.String char = "*";
        g.setColor(color);
        g.drawString("*", (int)p.get_x(), (int)p.get_y());
    }

    // private void drawLine(Line l){
    //     drawLine(l, new ColorRGB(1, 1, 1));
    // }


    private void drawLine(Line l, Color color) {
        //premier point de la Line
        Position p1 = l.get_start();
        Color c = color;/*new Color(color.get_R(),color.get_G(), color.get_B());*/
        int p1x = (this.minx < 0) ? (int)(p1.get_x() + Math.abs(this.minx)) :(int) p1.get_x() ;
        int p1y = (this.miny < 0) ? (int)(p1.get_y() + Math.abs(this.miny)) :(int) p1.get_y();
        //second point de la Line
        Position p2 = l.get_finish();
        int p2x = (this.minx < 0) ? (int)(p2.get_x() + Math.abs(this.minx)) :(int) p2.get_x();
        int p2y = (this.miny < 0) ? (int)(p2.get_y() + Math.abs(this.miny)) :(int) p2.get_y();
 
        Graphics2D g = get_g2d();
    
        g.setStroke(new BasicStroke(l.getStroke()));
        g.setColor(c);
    
        g.drawLine(p1x, p1y, p2x, p2y);
        
        
    }

    private Graphics2D get_g2d() {
        return this.g2d;
    }
  
}
