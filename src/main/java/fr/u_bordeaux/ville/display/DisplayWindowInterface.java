package fr.u_bordeaux.ville.display;
import java.awt.*;

import javax.swing.JFrame;

public interface DisplayWindowInterface {

    int getWidth();
    int getHeight();

    Color getBackgroundColor();
    Color getBuildingColor();
    Color getStreetColor();
    Color getDistrictColor();

    JFrame getFrameAwt();

}