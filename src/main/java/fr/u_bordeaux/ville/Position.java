package fr.u_bordeaux.ville;
public class Position implements Cloneable{

    private double _x;
    private double _y;

    public Position(double x, double y){
        this._x = x;
        this._y = y;
    }

    public void change_x(double widthBase,double widthChanged) {
        System.out.println(this._x);
        int x = (int)Math.ceil((this._x * widthChanged)/widthBase);
        this._x = (double)x +widthChanged;
        System.out.println(this._x);
    }

    public void change_y(double heightBase,double heightChanged) {
        System.out.println(this._y);
        int y= (int)Math.ceil((this._y * heightChanged)/heightBase);
        this._y =(double)y +heightChanged;
        System.out.println(this._y);
    }

    public double get_x() {
        return _x;
    }

    public double get_y() {
        return _y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()){
            return false;
        }
        Position p = (Position) obj;
        return p.get_x() == _x && p.get_y() == _y;
    }

    
    /** 
     * @return a copy of the position;
     */
    public Position clone(){
        Position tmp = null;
        try {
            tmp = (Position) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        tmp._x = _x;
        tmp._y = _y;
        return tmp;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("(" + (int)_x + ";" + (int)_y + ")");
        return string.toString();
    }

}
