package fr.u_bordeaux.ville;


import java.awt.Color;
import java.io.IOException;
import fr.u_bordeaux.ville.display.DisplayWindow;

public class App
{
	public static void main(String[] args) throws IOException
	{
		new DisplayWindow(800,500,new Color(4, 139, 154),new Color(255,255,255),new Color(0,0,0));
	}
}
