package fr.u_bordeaux.ville.parameters;

public interface Parameters {

    /**
     * @return the current angle
     */
    int getAngle();

    /**
     * @return the current max length
     */
    int maxLength();

    /**
     * @return the current min length
     */
    int minLength();


}
