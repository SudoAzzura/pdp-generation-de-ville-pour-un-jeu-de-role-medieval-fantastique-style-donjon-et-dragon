package fr.u_bordeaux.ville.parameters;

public class DistrictParameter extends AbtractParameters{

    public DistrictParameter(int _angle, int _maxLength, int _minLength) {
        super(_angle, _maxLength, _minLength);
    }
    
}
