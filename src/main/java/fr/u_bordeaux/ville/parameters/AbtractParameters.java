package fr.u_bordeaux.ville.parameters;

public class AbtractParameters implements Parameters{
    private int _angle;
    private int _maxLength;
    private int _minLength;

    
    public AbtractParameters(int _angle, int _maxLength, int _minLength) {
        this._angle = _angle;
        this._maxLength = _maxLength;
        this._minLength = _minLength;
    }

    @Override
    public int getAngle() {
        return _angle;
    }

    @Override
    public int maxLength() {
        return _maxLength;
    }

    @Override
    public int minLength() {
        return _minLength;
    }
    
}
