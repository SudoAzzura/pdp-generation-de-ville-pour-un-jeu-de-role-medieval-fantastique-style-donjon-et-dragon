package fr.u_bordeaux.ville;

public class ColorRGB implements Cloneable{
    private float _R;
    private float _G;
    private float _B;

    /** 
     * @param r red value 0.0 -1.0 inclusive
     * @param g green value 0.0 -1.0 inclusive
     * @param b blue value 0.0 -1.0 inclusive
     */
    public ColorRGB(float r, float g, float b) {
        this._R = r;
        this._G = g;
        this._B = b;
    }

    public float get_R() {
        return _R;
    }
    
    public float get_G() {
        return _G;
    }
    
    public float get_B() {
        return _B;
    }    

    public ColorRGB clone(){
        ColorRGB c = null;
        try {
            c = (ColorRGB) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        c._B = _B;
        c._G = _G;
        c._R = _R;
        return c;
    }
}
