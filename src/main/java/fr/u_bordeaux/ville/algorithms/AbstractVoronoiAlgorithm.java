package fr.u_bordeaux.ville.algorithms;

import java.util.List;

import de.alsclo.voronoi.graph.Point;

public abstract class AbstractVoronoiAlgorithm implements VoronoiAlgorithm
{

	private List<Point> _points;
	
	public AbstractVoronoiAlgorithm(List<Point> points)
	{
		this._points = points;
	}

	@Override
	public int getNbPoints()
	{
		return this._points.size();
	}

}
