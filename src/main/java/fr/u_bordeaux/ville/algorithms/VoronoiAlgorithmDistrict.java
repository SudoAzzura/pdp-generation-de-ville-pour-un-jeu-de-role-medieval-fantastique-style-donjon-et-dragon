package fr.u_bordeaux.ville.algorithms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Edge;
import de.alsclo.voronoi.graph.Graph;
import de.alsclo.voronoi.graph.Point;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;
import fr.u_bordeaux.ville.parameters.Parameters;
import fr.u_bordeaux.ville.urban_elements.Group;
import fr.u_bordeaux.ville.urban_elements.MainStreet;
import fr.u_bordeaux.ville.urban_elements.Street;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;

/**
 * Uses of de.alsclo.voronoi's algorithm and adapt it to our problem
 * @author tberasateguy
 *
 */
public class VoronoiAlgorithmDistrict implements Algorithm
{
	private List<Point> _points = null;
	private Voronoi _v = null;
	private UrbanElement _town = null; 
	private float _STROKE = 5;
	private int _xMin = 0;
	private int _xMax = 800;
	private int _yMin = 0;
	private int _yMax = 600;
	
	/**
	 * Create the object needed to create a Voronoi's diagram and then convert it to our data structure (UrbanElement)
	 * @param points - a List of points to create the Voronoi's diagram
	 * @param xMax - or width
	 * @param yMax - or height
	 */
	public VoronoiAlgorithmDistrict(List<Point> points, int xMax, int yMax)
	{
		this._points = points;
		this._v = new Voronoi(points);
		this._xMax = xMax;
		this._yMax = yMax;
	}
	
	/*/**
	 * Return a copy of the UrbanElement generated
	 * @return UrbanElement
	 */
	/*
	@Override
	public UrbanElement getElement()
	{
		return generate(null);
	}*/
	
	/**
	 * Return the Voronoi generated
	 * @return Voronoi
	 */
	public Voronoi getVoronoi()
	{
		return this._v;
	}
	
	/**
	 * Algorithm that generate the town sub-divided into districts
	 * @return UrbanElement
	 */
	/*	NEW VERSION : creates groups that represents the district, put the lines in it and finally put the district in the town*/
	private UrbanElement _generateTown()
	{
		UrbanElement town = new Group();
		
		Graph graph = this._v.getGraph();
		Set<Point> points = graph.getSitePoints();
		Set<Edge> edges = graph.edgeStream().collect(Collectors.toSet());
		Iterator<Edge> edgeIt = edges.iterator();
		
		Map<Point, Set<Edge>> map = new HashMap<>();
		
		while(edgeIt.hasNext())
		{
			Edge tmpEdge = edgeIt.next();
			if(!(tmpEdge.getA()==null || tmpEdge.getB()==null))
			{
				Point a = tmpEdge.getSite1();
				if(map.containsKey(a))
				{
					map.get(a).add(tmpEdge);
				}
				else
				{
					Set<Edge> setEdge = new HashSet<Edge>();
					setEdge.add(tmpEdge);
					map.put(a, setEdge);
				}
				
				Point b = tmpEdge.getSite2();
				if(map.containsKey(b))
				{
					map.get(b).add(tmpEdge);
				}
				else
				{
					Set<Edge> setEdge = new HashSet<Edge>();
					setEdge.add(tmpEdge);
					map.put(b, setEdge);
				}
			}
		}
		
		for(Map.Entry<Point, Set<Edge>> entry : map.entrySet())
		{
			Set<Edge> value = entry.getValue();
			if(value.size()>=2)
			{
				Set<Line> lines = new HashSet<Line>();
				value.forEach(edge -> {
					Point startPoint = edge.getA().getLocation();
					Point endPoint = edge.getB().getLocation();
					boolean isInside = this._isInsideBorders(startPoint) && this._isInsideBorders(endPoint);
//					isInside = true;
					if(isInside)
					{						
						lines.add(new Line(
								new Position(startPoint.x, startPoint.y),
								new Position(endPoint.x, endPoint.y)));
					}
				});
				UrbanElement district = this._createDistrict(lines);
				try {
					town.addChildren(district);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
/*
		
		for(Point tmpPoint1 : points)
		{
			List<Line> districtBorders = new ArrayList<Line>();
//			UrbanElement district = new Group();
			for(Point tmpPoint2 : points)
			{
				Optional<Edge> tmpEdge = graph.getEdgeBetweenSites(tmpPoint1, tmpPoint2);
				if(tmpEdge.isPresent() && tmpEdge.get().getB()!=null && tmpEdge.get().getA()!=null)
				{
					Edge realTmpEdge = tmpEdge.get();
					Point vStart = realTmpEdge.getA().getLocation();
//					if(realTmpEdge.getB()==null)
//					{
//						realTmpEdge.addVertex(new Vertex(this._findBorder(tmpPoint1, tmpPoint2, vStart)));
//					}
					Point vEnd = realTmpEdge.getB().getLocation();
					
					boolean isInside = this._isInsideBorders(vStart) && this._isInsideBorders(vEnd);
//					isInside = true;
					if(isInside)
					{			
						Position eStart = new Position(vStart.x, vStart.y);
						Position eEnd = new Position(vEnd.x, vEnd.y);
						Line eLine = new Line(eStart, eEnd, this._STROKE);
//						Street eStreet = new MainStreet(eLine);
						districtBorders.add(eLine);
//						try
//						{
//							district.addBorder(eStreet);
//						}
//						catch(IOException e)
//						{
//							e.printStackTrace();
//						}
					}
				}
			}
			
			//fermer le polygone
			UrbanElement district = this._createDistrict(districtBorders);			
			
			try
			{
				town.addChildren(district);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		*/
		return town;
	}
	
	/**
	 * OLD VERSION : only creates Lines and put them into the town
	 * @return
	 */
	@Deprecated
	private UrbanElement _generatTown()
	{
		UrbanElement town = new Group();
		Graph graph = this._v.getGraph();
		Iterator<Edge> it = graph.edgeStream().iterator();
		
		while(it.hasNext())
		{
			Edge tmpEdge = it.next();
			Point vStart = tmpEdge.getA().getLocation();
			Point vEnd = tmpEdge.getB().getLocation();

			Position eStart = new Position(vStart.x, vStart.y);
			Position eEnd = new Position(vEnd.x, vEnd.y);
			Line eLine = new Line(eStart, eEnd, this._STROKE);
			Street eStreet = new MainStreet(eLine);
			
			try
			{
				town.addBorder(eStreet);
			} catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		
		return town;
	}
	
	/**
	 * Implemented from Algorithm Interface
	 * Generate the UrbanElement if not generated and return a copy of it
	 */
	@Override
	public UrbanElement generate(Parameters p)
	{
		if(this._town==null)
		{
			this._town = _generateTown();
		}
		return this._town.clone();
	}
	
	/**
	 * With two Point (Voronoi's sites) given and a Point of an edge
	 * @param siteA 
	 * @param siteB
	 * @param startEdge
	 * @return the second Point of the edge starting from startEdge and intersecting the border 
	 */
	private Point _findBorder(Point siteA, Point siteB, Point startEdge)
	{
		//Point du segment par lequel passe la médiane
		Point midPoint = new Point((siteA.x+siteB.x)/2, (siteA.y+siteB.y)/2);
		
		//Coordonnées du segment AB
		Point coordAB = new Point(siteB.x-siteA.x, siteB.y-siteA.y);

		//Coordonnées manquantes des points des limites
		double x_yMin = this._findVectX(midPoint, coordAB, this._yMin);
		double x_yMax = this._findVectX(midPoint, coordAB, this._yMax);
		double y_xMin = this._findVectY(midPoint, coordAB, this._xMin);
		double y_xMax = this._findVectY(midPoint, coordAB, this._xMax);

		//Créé les 4 points d'intersection de la droite
		Map<Point, Double> map = new HashMap<>();
		Point a = new Point(this._xMin, y_xMin); 
		Point b = new Point(this._xMax, y_xMax);
		Point c = new Point(x_yMin, this._yMin);
		Point d = new Point(x_yMax, this._yMax);
		map.put(a, this._distance(midPoint, a));
		map.put(b, this._distance(midPoint, b));
		map.put(c, this._distance(midPoint, c));
		map.put(d, this._distance(midPoint, d));
		
		return this._closerPoint(map);
	}

	/**
	 * 
	 * @param midPoint
	 * @param coordsAB
	 * @param y
	 * @return the X coordinate of a function given by the params
	 */
	private double _findVectX(Point midPoint, Point coordsAB, double y)
	{
		//IM = (x-mid.x, y-mid.y)
		//IM*AB = (x-mid.x)*AB.x + (y-mid.y)*AB.y
		//(x-mid.x)*AB.x = -((y-mid.y)*AB.y)
		//x-mid.x = -((y-mid.y)*AB.y)/AB.x
		//x =  (-((y-mid.y)*AB.y)/AB.x) + mid.x
		return (-((y-midPoint.y)*coordsAB.y)/coordsAB.x) + midPoint.x;
		//return ((-(y-midPoint.y)*coordsAB.y)/coordsAB.x)+midPoint.x;
	}
	
	/**
	 * 
	 * @param midPoint
	 * @param coordsAB
	 * @param x
	 * @return the Y coordinate of a function given by the params
	 */
	private double _findVectY(Point midPoint, Point coordsAB, double x)
	{
		//IM = (x-mid.x, y-mid.y)
		//IM*AB = (x-mid.x)*AB.x + (y-mid.y)*AB.y
		//(y-mid.y)*AB.y = -((x-mid.x)*AB.x) 
		//y-mid.y = -((x-mid.x)*AB.x)/AB.y
		//y = (-((x-mid.x)*AB.x)/AB.y)+mid.y
		return (-((x-midPoint.x)*coordsAB.x)/coordsAB.y) + midPoint.y;
		//return ((-(x-midPoint.x)*coordsAB.x)/coordsAB.y)+midPoint.y;
	}
	
	/**
	 * Calculate the Euclidean distance of two Points
	 * @param x1
	 * @param x2
	 * @return the distance
	 */
	private double _distance(Point x1, Point x2)
	{
		return Math.sqrt(Math.pow(x1.x-x2.x, 2)+Math.pow(x1.y-x2.y, 2));
	}
	
	/**
	 * Find the point with the smaller distance
	 * @param map
	 * @return the Point
	 */
	private Point _closerPoint(Map<Point, Double> map)
	{
		double min = Double.MAX_VALUE;
		Point tmpPoint = null;
		
		for(Point tmp : map.keySet())
		{
			double dist = map.get(tmp); 
			if(dist<min)
			{
				min = dist;
				tmpPoint = tmp;
			}
		}
		
		return tmpPoint;
	}
	
	/**
	 * Calculate if a Point is inside the border
	 * @param p
	 * @return true if the Point is inside the border
	 */
	private boolean _isInsideBorders(Point p)
	{
		return p.x>this._xMin && p.x<this._xMax && p.y>this._yMin && p.y<this._yMax;
	}
	
	private UrbanElement _createDistrict(Collection<Line> borders)
	{
		UrbanElement district = new Group();
		
		List<Position> positions = new ArrayList<Position>();
		
		for(Line line : borders)
		{
			try
			{
				district.addBorder(new MainStreet(line.get_start(), line.get_finish(), this._STROKE));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			
			Position start = line.get_start();
			Position end = line.get_finish();
			
			if(positions.contains(start))
			{	
				positions.remove(positions.indexOf(start));
			}
			else
			{	
				positions.add(start);
			}
			
			if(positions.contains(end))
			{
				positions.remove(positions.indexOf(end));
			}
			else
			{	
				positions.add(end);
			}			
		}
		
		if(positions.size()!=2)
		{
			return district;
		}
		
		try
		{
			Street tmpStreet = new MainStreet(positions.get(0), positions.get(1), this._STROKE);
			district.addBorder(tmpStreet);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return district;
	}
}
