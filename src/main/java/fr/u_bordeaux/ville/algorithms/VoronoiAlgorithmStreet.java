package fr.u_bordeaux.ville.algorithms;

import java.awt.Polygon;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Edge;
import de.alsclo.voronoi.graph.Graph;
import de.alsclo.voronoi.graph.Point;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;
import fr.u_bordeaux.ville.parameters.Parameters;
//import fr.u_bordeaux.ville.urban_elements.Group;
import fr.u_bordeaux.ville.urban_elements.MainStreet;
import fr.u_bordeaux.ville.urban_elements.Street;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;

public class VoronoiAlgorithmStreet extends AbstractVoronoiAlgorithm
{
	private UrbanElement _town = null;
	private UrbanElement _newTown = null;
	private Voronoi _v = null;
	private float _STROKE = 1; // ???
	private int _xMin = 0;
	private int _xMax = 800;
	private int _yMin = 0;
	private int _yMax = 600;
	
	/**
	 * 
	 * @param points
	 * @param town
	 */
	public VoronoiAlgorithmStreet(List<Point> points, int xMax, int yMax, UrbanElement town)
	{
		super(points);
		this._v = new Voronoi(points);
		this._town = town;
		this._xMax = xMax;
		this._yMax = yMax;
	}

	@Override
	public Voronoi getVoronoi()
	{
		return this._v;
	}

	@Override
	public UrbanElement generate(Parameters p)
	{
		if(this._newTown==null)
		{
			this._newTown = this._generateDistrict();
		}
		return this._newTown.clone();
	}
	
	private UrbanElement _generateDistrict()
	{
		Graph graph = this._v.getGraph();
		Set<Edge> edges = graph.edgeStream().collect(Collectors.toSet());
		
		Iterator<UrbanElement> districtIterator = this._town.getChildren();
		while(districtIterator.hasNext())
		{
			UrbanElement district = districtIterator.next();
			Polygon districtBorders = this._districtBorders(district);	
			
			Set<Edge> clonedEdges = new HashSet<Edge>(edges);
			Iterator<Edge> edgeIt = clonedEdges.iterator();
			while(edgeIt.hasNext())
			{
				Edge tmpEdge = edgeIt.next();
				if(tmpEdge.getB()!=null && tmpEdge.getA()!=null)
				{
					Point vStart = tmpEdge.getA().getLocation();
					Point vEnd = tmpEdge.getB().getLocation();
					
					boolean isInside = districtBorders.contains(new java.awt.Point((int)vStart.x, (int)vStart.y)) || districtBorders.contains(new java.awt.Point((int)vEnd.x, (int)vEnd.y));//districtBorders.contains(new java.awt.Point((int)tmpPoint1.x, (int)tmpPoint1.y)) || districtBorders.contains(new java.awt.Point((int)tmpPoint2.x, (int)tmpPoint2.y));
					Position eStart = new Position(vStart.x, vStart.y);
					Position eEnd = new Position(vEnd.x, vEnd.y);
					isInside = true;
					if(isInside)
					{			
						Line eLine = new Line(eStart, eEnd, this._STROKE);
						Street eStreet = new MainStreet(eLine);
						try
						{
							this._town.addChildren(eStreet);
						}
						catch(IOException e)
						{
							e.printStackTrace();
						}
						edges.remove(tmpEdge);
					}
				}
			}
		}
		
		return this._town;
	}
	
	private Polygon _districtBorders(UrbanElement district)
	{
		Iterator<Street> districtBordersIterator = district.getBorders();
		Polygon districtBorders = new Polygon();
		while(districtBordersIterator.hasNext())
		{
			Line tmpLine = districtBordersIterator.next().getLine();
			Position tmpStart = tmpLine.get_start();
			Position tmpEnd = tmpLine.get_finish();
			districtBorders.addPoint((int)tmpStart.get_x(), (int)tmpStart.get_y());
			districtBorders.addPoint((int)tmpEnd.get_x(), (int)tmpEnd.get_y());
		}
		return districtBorders;
	}

}
