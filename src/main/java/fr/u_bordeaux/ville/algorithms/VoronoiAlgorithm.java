package fr.u_bordeaux.ville.algorithms;

import de.alsclo.voronoi.Voronoi;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;

public interface VoronoiAlgorithm extends Algorithm
{
	public int getNbPoints();
	public Voronoi getVoronoi();
	//public UrbanElement getElement(); // Pas nécessaire ???
}
