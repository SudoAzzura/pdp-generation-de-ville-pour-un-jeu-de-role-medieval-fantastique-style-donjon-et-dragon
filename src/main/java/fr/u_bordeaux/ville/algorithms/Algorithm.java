package fr.u_bordeaux.ville.algorithms;

import fr.u_bordeaux.ville.parameters.Parameters;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;

public interface Algorithm {
    
    UrbanElement generate(Parameters p);
}
