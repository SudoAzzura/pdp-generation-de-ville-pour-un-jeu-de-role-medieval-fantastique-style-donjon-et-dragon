package fr.u_bordeaux.ville.algorithms.PointAlgorithms;

import java.util.ArrayList;
import java.util.List;

import de.alsclo.voronoi.graph.Point;

public class PointAlgorithmRounded implements PointAlgorithm
{
	private List<Point> _points = new ArrayList<Point>();
	private double _radius;
	
	public PointAlgorithmRounded(int nbPoints, double radius, int xMax, int yMax)
	{
		this._radius = radius;
		double angle = 2*Math.PI/nbPoints;

		for(int i=0; i<nbPoints; i++)
		{
			double x = this._radius*Math.cos(angle*i)-this._radius+xMax/2;
			double y = this._radius*Math.sin(angle*i)-this._radius+yMax/2;
			if(x<xMax && y<yMax)
			{
				this._points.add(new Point(x, y));
			}		
			else
			{
				System.out.println("*************************************");
			}
		}
		
		this._radius*=2;
		
		for(int i=0; i<nbPoints; i++)
		{
			double x = this._radius*Math.cos(angle*i)-this._radius/2+xMax/2;
			double y = this._radius*Math.sin(angle*i)-this._radius/2+yMax/2;
			if(x<xMax && y<yMax)
			{
				this._points.add(new Point(x, y));
			}
			else
			{
				System.out.println("*************************************");
			}
		}

	}

	@Override
	public List<Point> getPoints()
	{
		return this._points;
	}
	
}
