package fr.u_bordeaux.ville.algorithms.PointAlgorithms;

import java.util.List;

import de.alsclo.voronoi.graph.Point;

public interface PointAlgorithm
{
	List<Point> getPoints();
}
