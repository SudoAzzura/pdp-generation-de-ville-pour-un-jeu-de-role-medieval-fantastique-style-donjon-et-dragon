package fr.u_bordeaux.ville.algorithms.PointAlgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.alsclo.voronoi.graph.Point;

public class PointAlgorithmRandom implements PointAlgorithm
{
	List<Point> _list = new ArrayList<>();
	
	public PointAlgorithmRandom(int nbDistricts, int xMin, int xMax, int yMin, int yMax)
	{
		Random r = new Random();
		int dx = xMax-xMin;
		int dy = yMax-yMin;
		for(int i=0; i<nbDistricts; i++)
		{
			
			Point p = new Point(r.nextInt(dx)+xMin, r.nextInt(dy)+yMin);
			System.out.print(p.x+" "+p.y+"\n");
			this._list.add(p);
		}
		
//		this._list.add(new Point(312, 489));
//		this._list.add(new Point(210, 254));
//		this._list.add(new Point(32, 354));
//		this._list.add(new Point(375, 501));
//		this._list.add(new Point(405, 506));
//		this._list.add(new Point(66, 385));
//		this._list.add(new Point(509, 515));
	}

	@Override
	public List<Point> getPoints()
	{
		return this._list;
	}
}
