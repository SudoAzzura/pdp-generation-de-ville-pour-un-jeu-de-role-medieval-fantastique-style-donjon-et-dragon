package fr.u_bordeaux.ville.algorithms.PointAlgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.lang.Math;

import de.alsclo.voronoi.graph.Point;

public class PointAlgorithmSquared implements PointAlgorithm
{
	
	List<Point> _list = new ArrayList<>();
	
	public PointAlgorithmSquared(int nbPoints, int xMax, int yMax)
	{
		Random r = new Random(); 
		double cos = r.nextDouble(); // Cos aléatoire

		double xDep = xMax / 2;
		double yDep = yMax / 2;

		Random r2 = new Random();
		double sSaut = r2.nextDouble() * 200 + 100;

		/*
		for (int i = 0; i < Math.round(Math.sqrt(nbPoints)); i++){
			for (int k = 0; k < Math.round(Math.sqrt(nbPoints)); k++){*/
		for (int i = 0; i < nbPoints; i++){
			for (int k = 0; k < nbPoints; k++){	
				System.out.println("x = " + (xDep + ((double) i) * sSaut * cos) + " y  = " + (yDep + ((double) k) * sSaut * Math.sqrt(1 - cos*cos)));
				Point p = new Point(xDep + ((double) i) * sSaut * cos, yDep + ((double) k) * sSaut * Math.sqrt(1 - cos*cos));
				this._list.add(p);
			}
		}	
	}
	
	@Override
	public List<Point> getPoints()
	{
		return this._list;
	}

}
