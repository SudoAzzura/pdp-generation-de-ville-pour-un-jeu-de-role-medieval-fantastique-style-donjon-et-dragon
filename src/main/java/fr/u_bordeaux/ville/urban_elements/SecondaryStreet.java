package fr.u_bordeaux.ville.urban_elements;

import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public class SecondaryStreet extends Street{

    public SecondaryStreet(Line ligne) {
        super(ligne);
    }

    public SecondaryStreet(Position start, Position finish){
       super(start, finish); 
    }

    public SecondaryStreet(Position start, Position finish, Float stroke){
        super(start, finish, stroke); 
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("SecondaryStreet : ");
        string.append(super.toString());
        return string.toString();
    }

    
    
}
