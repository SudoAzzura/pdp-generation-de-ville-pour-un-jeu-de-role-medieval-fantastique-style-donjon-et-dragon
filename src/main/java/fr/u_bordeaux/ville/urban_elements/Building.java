package fr.u_bordeaux.ville.urban_elements;

import java.util.ArrayList;
import java.util.Iterator;

import fr.u_bordeaux.ville.Position;

public class Building extends AbstractUrbanElement{

    private Position _p;

    public Building (Position p){
        super();
        this._p = p.clone();
    }

    public Building(double x, double y){
        super();
        this._p = new Position(x, y);
    }

    @Override
    public Building clone(){
        Building b = (Building) super.clone();
        b._p = _p.clone();
        return b;
    }

    @Override
    public boolean IsInBorder(Position p) {
        return p == this._p;
    }

    @Override
    public Iterator<Position> getPointsNotOnBorder() {
        ArrayList<Position> t = new ArrayList<Position>();
        t.add(_p.clone());
        return t.iterator();
    }
    
}
