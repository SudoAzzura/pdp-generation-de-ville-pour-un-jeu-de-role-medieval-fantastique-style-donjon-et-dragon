package fr.u_bordeaux.ville.urban_elements;

import java.io.IOException;
import java.util.Iterator;

import fr.u_bordeaux.ville.ColorRGB;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public abstract class AbstractUrbanElement implements UrbanElement{

    private ColorRGB _color = null;

    public AbstractUrbanElement() {
        super();
        _color = new ColorRGB(0, 0, 1);
    }

    @Override
    public UrbanElement clone() {
        AbstractUrbanElement tmp = null;
        try {
            tmp =  (AbstractUrbanElement) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        tmp._color = _color.clone();
        return tmp;
    }

    @Override
    public UrbanElement addBorder(Street s) throws IOException {
        throw new IOException();
    }

    @Override
    public UrbanElement addChildren(UrbanElement e) throws IOException {
        throw new IOException();
    }

    @Override
    public Iterator<UrbanElement> getChildren() {
        return null;
    }

    
    @Override
    public UrbanElement removeChildren(UrbanElement e) {
        return this;
    }

    @Override
    public Iterator<Street> getBorders() {
        return null;
    }

    @Override
    public Iterator<Line> getAllChildrenLines() {
        return null;
    }

    @Override
    public Iterator<Position> getPointsNotOnBorder() {
        return null;
    }

    @Override
    public UrbanElement removeBorder(Street e) {
        return this;
    }

    @Override
    public ColorRGB getColor() {
        return _color.clone();
    }

    @Override
    public UrbanElement setColor(float r, float g, float b) {
        this._color = new ColorRGB(r,g,b);
        return this;
    }

    @Override
    public Iterator<Street> getAllBordersStreets() {
        return null;
    }
    

}
