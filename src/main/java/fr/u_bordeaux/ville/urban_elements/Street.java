package fr.u_bordeaux.ville.urban_elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public abstract class Street extends AbstractUrbanElement{
    private Line _l;


    public Street(Line ligne) {
        super();
        this._l = ligne;
    }

    public Street(Position start, Position finish, float stroke){
        super();
        this._l = new Line(start, finish, stroke);
    }

    public Street(Position start, Position finish){
        super();
        this._l = new Line(start, finish);
    }

    
    @Override
    public Street clone() {
        Street tmp = null;
        tmp = (Street) super.clone();
        tmp._l = _l.clone();
        return tmp;
    }

    @Override
    public boolean IsInBorder(Position p) {
        return _l.isIn(p);
    }


    @Override
    public Iterator<Street> getBorders() {
        ArrayList<Street> t = new ArrayList<Street>();
        t.add(this.clone());
        return t.iterator();
    }

    @Override
    public Iterator<UrbanElement> getChildren() {
        return null;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append(_l);
        return string.toString();
    }

    public Line getLine() {
        return _l.clone();
    }

    @Override
    public Iterator<Line> getAllChildrenLines() {
        ArrayList<Line> t = new ArrayList<Line>();
        t.add(_l.clone());
        return t.iterator();
    }

    
    
    
}
