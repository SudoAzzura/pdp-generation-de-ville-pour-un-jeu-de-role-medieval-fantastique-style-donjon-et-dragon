package fr.u_bordeaux.ville.urban_elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public class Group extends AbstractUrbanElement{

    private ArrayList<UrbanElement> _children = new ArrayList<UrbanElement>();
    private ArrayList<Street> _borders = new ArrayList<Street>();

    public Group(){
        super();
    }
    
    @Override
    public boolean IsInBorder(Position p) {
        for (UrbanElement urbanElement : _children) {
            if(!urbanElement.IsInBorder(p)){
                return false;
            }
        }
        return true;
    }
    
    
    @Override
    public UrbanElement addChildren(UrbanElement e) throws IOException{
        if (e == this) throw new IOException();
        _children.add(e);
        return this;
    }

    @Override
    public UrbanElement addBorder(Street s) throws IOException{
        _borders.add(s);
        return this;
    }

    @Override
    public UrbanElement removeChildren(UrbanElement e) {
        _children.remove(e);
        return this;
    }

    
    
    @Override
    public Group clone() {
        Group tmp = null;
        tmp = (Group) super.clone();
        tmp._children = cloneChidren();
        return tmp;
    }

    @Override
    public Iterator<Line> getAllChildrenLines() {
        Iterator<Line> i;
        ArrayList<Line> t = new ArrayList<Line>();
        for (UrbanElement urbanElement : _children) {
            i = urbanElement.getAllChildrenLines();
            while(i.hasNext()){
                t.add(i.next());
            }
        }
        return t.iterator();
    }

    @Override
    public Iterator<Street> getBorders() {
        ArrayList<Street> t = new ArrayList<Street>();
        for (Street street : _borders) {
            t.add(street.clone());
        }
        return t.iterator();
    }


    @Override
    public Iterator<UrbanElement> getChildren() {
        return cloneChidren().iterator();
    }

    private ArrayList<UrbanElement> cloneChidren(){
        ArrayList<UrbanElement> t = new ArrayList<UrbanElement>();
        for (UrbanElement urbanElement : _children) {
            t.add(urbanElement.clone());
        }
        return t;
    }



    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("group :\n");
        for (UrbanElement urbanElement : _children) {
            string.append("   " + urbanElement + "\n");
        }
        return string.toString();
    }



    @Override
    public Iterator<Position> getPointsNotOnBorder() {
        Iterator<Position> i;
        ArrayList<Position> t = new ArrayList<Position>();
        for (UrbanElement urbanElement : _children) {
            i = urbanElement.getPointsNotOnBorder();
            while(i.hasNext()){
                t.add(i.next());
            }
        }
        return t.iterator();
    }


    @Override
    public UrbanElement removeBorder(Street street) {
        _borders.remove(street);
        return null;
    }

    @Override
    public UrbanElement setColor(float r, float g, float b) {
        super.setColor(r, g, b);
        for (UrbanElement urbanElement : _children) {
            urbanElement.setColor(r, g, b);
        }
        return this;
    }

    @Override
    public Iterator<Street> getAllBordersStreets() {
        ArrayList<Street> streets = new ArrayList<Street>();
        for (Street street : _borders) {
            streets.add(street);
        }
        for (UrbanElement urbanElement : _children) {
            Iterator<Street> childStreets = urbanElement.getAllBordersStreets();
            if(childStreets==null) continue;
            while(childStreets.hasNext()){
                streets.add(childStreets.next());
            }
        }
        return streets.iterator();
    }

    
}
