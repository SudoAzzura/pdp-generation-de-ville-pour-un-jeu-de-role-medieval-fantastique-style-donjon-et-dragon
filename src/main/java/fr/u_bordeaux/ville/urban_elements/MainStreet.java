package fr.u_bordeaux.ville.urban_elements;

import java.util.Iterator;

import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public class MainStreet extends Street{


    
    public MainStreet(Line ligne) {
        super(ligne);
    }
    
    public MainStreet(Position start, Position finish){
        super(start, finish);
    }

    public MainStreet(Position start, Position finish, float stroke){
        super(start, finish, stroke);
    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("MainStreet : ");
        string.append(super.toString());
        return string.toString();
    }
    
    
}
