package fr.u_bordeaux.ville.urban_elements;

import java.io.IOException;
import java.util.Iterator;

import fr.u_bordeaux.ville.ColorRGB;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;

public interface UrbanElement extends Cloneable{
    
    /** 
     * @param p
     * @return true if the position is in the element
     * For a group : return true if the position is in ALL elements
     */
    boolean IsInBorder(Position p);


    /** 
     * @return all line composing the group or the element
     */
    public Iterator<Line> getAllChildrenLines();

    /** 
     * @return all line bounding the group or the element
     */
    public Iterator<Street> getAllBordersStreets();

    /** 
     * @return all line bounding the group or the element
     */
    public Iterator<Street> getBorders();

    /** 
     * @return all position composing the group or the element without those composing the borders.
     */
    Iterator<Position> getPointsNotOnBorder();

    ColorRGB getColor();
    UrbanElement setColor(float r, float g, float b);

    //Composite


    /** 
     * @param e the element to add
     * @return urban element on witch it's called
     * @throws IOException : should only be called on a group, should not be called on itself
     */
    UrbanElement addChildren(UrbanElement e) throws IOException;


    /** 
     * @param s the street to add
     * @return urban element on witch it's called
     * @throws IOException : should only be called on a group, should not be called on itself
     */
    public UrbanElement addBorder(Street s) throws IOException;

    /** 
     * @param the element to remove
     * @return urban element on witch it's called
     */
    UrbanElement removeChildren(UrbanElement e);

     /** 
     * @param the element to remove
     * @return urban element on witch it's called
     */
    UrbanElement removeBorder(Street e);

    /**
     * @return  a list of clones of all elements of the group 
     */
    Iterator<UrbanElement> getChildren();

    
    //prototype

    /** 
     * @return a perfect copy of the element
     */
    UrbanElement clone();

  
}
