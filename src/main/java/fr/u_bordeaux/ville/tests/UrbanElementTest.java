package fr.u_bordeaux.ville.tests;
import de.alsclo.voronoi.graph.Point;
import fr.u_bordeaux.ville.Line;
import fr.u_bordeaux.ville.Position;
import fr.u_bordeaux.ville.algorithms.Algorithm;
import fr.u_bordeaux.ville.algorithms.VoronoiAlgorithmDistrict;
import fr.u_bordeaux.ville.display.Display;
import fr.u_bordeaux.ville.urban_elements.Street;
import fr.u_bordeaux.ville.urban_elements.UrbanElement;
import fr.u_bordeaux.ville.display.DisplayAwt;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class UrbanElementTest {
    private static UrbanElement Town;
    private static final int max = 100;
    private static final int nbDistricts = 25;
    private static  ArrayList<Point> list_Points;

    @BeforeAll
    static void setUp() {
        list_Points = new ArrayList<>();
        list_Points.add(new Point(42.0,43.0));
        list_Points.add(new Point(4.0,97.0));
        list_Points.add(new Point(34.0,47.0));
        list_Points.add(new Point(74.0,69.0));
        list_Points.add(new Point(54.0,65.0));
        list_Points.add(new Point(16.0,48.0));
        list_Points.add(new Point(7.0,54.0));
        list_Points.add(new Point(41.0,23.0));
        list_Points.add(new Point(2.0,49.0));
        list_Points.add(new Point(88.0,86.0));
        list_Points.add(new Point(7.0,4.0));
        list_Points.add(new Point(79.0,84.0));
        list_Points.add(new Point(38.0,0.0));
        list_Points.add(new Point(56.0,28.0));
        list_Points.add(new Point(69.0,83.0));
        list_Points.add(new Point(98.0,50.0));
        list_Points.add(new Point(59.0,90.0));
        list_Points.add(new Point(95.0,68.0));
        list_Points.add(new Point(72.0,71.0));
        list_Points.add(new Point(25.0,34.0));
        list_Points.add(new Point(37.0,9.0));
        list_Points.add(new Point(96.0,66.0));
        list_Points.add(new Point(36.0,51.0));
        list_Points.add(new Point(83.0,96.0));
        list_Points.add(new Point(15.0,80.0));
        Algorithm va = new VoronoiAlgorithmDistrict(list_Points, max, max);
        Town = va.generate(null);
    }
    @Test
    @DisplayName("UrbanElement Test : Isin Test")
    void IsInBorder() {
        assertAll(
                () -> assertEquals(true,this.Town.IsInBorder(new Position(50, 50))), // check if is inside town
                () -> assertEquals(false,this.Town.IsInBorder(new Position(-600, -6000))) // check if is not in town
        );
    }


    @Test
    @DisplayName("UrbanElement Test : getBorders ")
    void getBorders() {
        // On récupère les Bordures
            Iterator<Street> Borders = this.Town.getAllBordersStreets();
            ArrayList<Line> L = new ArrayList<>();
            while (Borders.hasNext()){
                UrbanElement d = Borders.next();
                Iterator<Street> s = d.getBorders();
                s.forEachRemaining(l -> L.add(l.getLine()));
            }
        // On vérifie qu'il y a au minimum 3 bordures
            assertAll(
                    () -> assertFalse(L.isEmpty()),
                    () -> assertTrue((L.size() >= 3))
            );
        // On vérifie que les bordures sont réliées entres elles
            int k = 0;
            for(Line l1:L){
                for (Line l2:L){
                    if(!l1.equals(l2) && l1.intersect(l2)){
                        k++;
                    }
                }
                assertTrue(k>0);
                //System.out.println("K : "+k+" for : "+l1);
                k = 0;
            }
    }
}