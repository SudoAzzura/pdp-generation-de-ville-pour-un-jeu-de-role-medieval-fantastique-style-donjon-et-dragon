package fr.u_bordeaux.ville.tests;
import fr.u_bordeaux.ville.parameters.DistrictParameter;
import fr.u_bordeaux.ville.parameters.SteetParameter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParametersTest {

    @Test
    @DisplayName("Parameters : getAngle test")
    void getAngle() {
        // cas normal
        SteetParameter sp_1 = new SteetParameter(12,23,80);
        DistrictParameter dp_1 = new DistrictParameter(12,23,80);
        //cas extreme positif
        SteetParameter sp_2 = new SteetParameter(Integer.MAX_VALUE,0,0);
        DistrictParameter dp_2 = new DistrictParameter(Integer.MAX_VALUE,0,0);
        // cas extreme negatif
        SteetParameter sp_3 = new SteetParameter(Integer.MIN_VALUE,0,0);
        DistrictParameter dp_3 = new DistrictParameter(Integer.MIN_VALUE,0,0);
        assertAll(
                () -> assertEquals(12,sp_1.getAngle()),
                () -> assertEquals(12,dp_1.getAngle()),
                () -> assertEquals(Integer.MAX_VALUE,sp_2.getAngle()),
                () -> assertEquals(Integer.MAX_VALUE,dp_2.getAngle()),
                () -> assertEquals(Integer.MIN_VALUE,sp_3.getAngle()),
                () -> assertEquals(Integer.MIN_VALUE,dp_3.getAngle())
        );
    }

    @Test
    @DisplayName("Parameters : maxLength test")
    void maxLength() {
        // cas normal
        SteetParameter sp_1 = new SteetParameter(12,23,80);
        DistrictParameter dp_1 = new DistrictParameter(12,23,80);
        //cas extreme positif
        SteetParameter sp_2 = new SteetParameter(0,Integer.MAX_VALUE,0);
        DistrictParameter dp_2 = new DistrictParameter(0,Integer.MAX_VALUE,0);
        // cas extreme negatif
        SteetParameter sp_3 = new SteetParameter(0,Integer.MIN_VALUE,0);
        DistrictParameter dp_3 = new DistrictParameter(0,Integer.MIN_VALUE,0);
        assertAll(
                () -> assertEquals(23,sp_1.maxLength()),
                () -> assertEquals(23,dp_1.maxLength()),
                () -> assertEquals(Integer.MAX_VALUE,sp_2.maxLength()),
                () -> assertEquals(Integer.MAX_VALUE,dp_2.maxLength()),
                () -> assertEquals(Integer.MIN_VALUE,sp_3.maxLength()),
                () -> assertEquals(Integer.MIN_VALUE,dp_3.maxLength())
        );
    }

    @Test
    @DisplayName("Parameters : minLength test")
    void minLength() {
        // cas normal
        SteetParameter sp_1 = new SteetParameter(12,23,80);
        DistrictParameter dp_1 = new DistrictParameter(12,23,80);
        //cas extreme positif
        SteetParameter sp_2 = new SteetParameter(0,0,Integer.MAX_VALUE);
        DistrictParameter dp_2 = new DistrictParameter(0,0,Integer.MAX_VALUE);
        // cas extreme negatif
        SteetParameter sp_3 = new SteetParameter(0,0,Integer.MIN_VALUE);
        DistrictParameter dp_3 = new DistrictParameter(0,0,Integer.MIN_VALUE);
        assertAll(
                () -> assertEquals(80,sp_1.minLength()),
                () -> assertEquals(80,dp_1.minLength()),
                () -> assertEquals(Integer.MAX_VALUE,sp_2.minLength()),
                () -> assertEquals(Integer.MAX_VALUE,dp_2.minLength()),
                () -> assertEquals(Integer.MIN_VALUE,sp_3.minLength()),
                () -> assertEquals(Integer.MIN_VALUE,dp_3.minLength())
        );
    }
}