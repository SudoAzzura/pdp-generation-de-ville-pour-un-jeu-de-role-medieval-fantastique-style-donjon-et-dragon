package fr.u_bordeaux.ville;

public class Line implements Cloneable{
    
    private Position _start;
    private Position _finish;
    private double _coef;
    private double _lengh;
    private double _offset;
    private float _stroke = 1;

    public Line(Position start, Position finish, float stroke){
        this._start = start;
        this._finish = finish;
        this._stroke = stroke;
        computeLigneEquation();
        computeLengh();
    }

    /** 
     * By default the stroke is set to 1
     */
    public Line(Position start, Position finish) {
        this._start = start;
        this._finish = finish;
        computeLigneEquation();
        computeLengh();
    }

    public float getStroke()
    {
    	return this._stroke;
    }

    
    /** 
     * @param l
     * @return Position of the intersection if there is such point
     * @return null otherwise
     */
    public Position getIntersection(Line l){
        double x = (l.get_offset() - _offset) / (_coef - l.get_coef());
        double y = _coef * x + _offset;
        Position p =  new Position(x, y); 

        if (isIn(p) && l.isIn(p)){
            return p;
        }
        return null;
    }

    /** 
     * @param l a line
     * @return true if the lines intersects false otherwise
     */
    public boolean intersect(Line l){
        return getIntersection(l) != null;
    }

    
    /** 
     * @param p a position
     * @return true if the point is on the line false otherwise
     */
    public boolean isIn(Position p){
        double xmin = Math.min(_start.get_x(), _finish.get_x());
        double xmax = Math.max(_start.get_x(), _finish.get_x());

        double ymin = Math.min(_start.get_y(), _finish.get_y());
        double ymax = Math.max(_start.get_y(), _finish.get_y());
        return ((p.get_x() >= xmin) && (p.get_x() <= xmax) && (p.get_y() >= ymin) && (p.get_y() <= ymax));
    }

    
    /** 
     * @param l a line
     * @return the value of the minimal angle between this and @param l
     */
    //source calcul de l'angle : https://www.nagwa.com/fr/explainers/407162748438/
    public double angle(Line l){
        if (_coef == l.get_coef()){
            //paralleles
            return 0.;
        }
        if (_coef * l.get_coef() == -1){
            //perpendiculaires
            return 90.;
        }
        return Math.atan(Math.abs((_coef - l.get_coef()) / (1 + _coef*l.get_coef())));
    }

    public double get_coef(){
        return _coef;
    }

    public Position get_start() {
        return _start;
    }

    public Position get_finish() {
        return _finish;
    }

    public double get_lengh() {
        return _lengh;
    }

    public double get_offset() {
        return _offset;
    }

    
    /** 
     * @return the value of the greater x coordinate between start and finish points of the line.
     */
    public double getXmax(){
        return Math.max(_start.get_x(), _finish.get_x());
    }

    //source calcul longueur : https://www.alloprof.qc.ca/fr/eleves/bv/mathematiques/math-la-distance-entre-deux-points-m1311
    private void computeLengh(){
        double dx = _start.get_x() - _finish.get_x(); 
        double dy = _start.get_y() - _finish.get_y();
        this._lengh = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        
    }

    //sources calcul du coef : https://www.mathforu.com/seconde/determiner-equation-droite/
    private void computeLigneEquation(){
        double dx = _start.get_x() - _finish.get_x(); 
        double dy = _start.get_y() - _finish.get_y(); 
        this._coef =  dy / dx;
        this._offset = _start.get_y() - _coef*_start.get_x();
    }


    
    /** 
     * @return a copy of the line
     */
    public Line clone(){
        Line tmp = null;
        try {
            tmp = (Line) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        tmp._start = (Position) _start.clone();
        tmp._finish = (Position) _finish.clone();
        tmp._coef = _coef;
        tmp._lengh = _lengh;
        tmp._offset = _offset;
        return tmp;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("(" + _start + ";" + _finish + ";" + (int)_stroke + ")");
        return string.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()){
            return false;
        }
        Line l = (Line) obj;
        return l._stroke == this._stroke &&
            ((l._start == this._start && l._finish == this._finish) ||
            (l._finish == this._start && l._start == this._finish));
    }
    

    

}
