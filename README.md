# Pdp - DnD Town Generator

Pdp's project about a town generator for medieval/fantastic rpg like DnD

## Installation
For using this project, you may use maven.
You can either use the two scripts given *compile* and *launch*,
OR use the commands
1. `mvn clean` to be sure the environment doesn't have any old file.
2. `mvn compile` to compile the sources.
3. `mvn exec:java -Dexec.mainClass="fr.u_bordeaux.ville.App"` to execute the project.

## Authors and acknowledgment
BERASATEGUY Tanguy
CHOUCHANE Hichem
DIAZ RECHE Julio
DOUCET Maxence
NASH Deborah
NEUVILLE Chloe

Uses of [aschlosser's Voronoi generator](https://github.com/aschlosser/voronoi-java)