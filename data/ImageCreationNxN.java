import java.io.IOException;

class ImageCreationNxN {
	  	
	public static void main(String[] args) throws IOException {
		javax.swing.JFrame frame = new javax.swing.JFrame();
		
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		int size = 1000;
		java.awt.Image img = createSampleImage(size);
		javax.swing.ImageIcon icon = new javax.swing.ImageIcon(img);
		
		frame.add(new javax.swing.JLabel(icon));
		frame.pack();
		frame.setVisible(true);
		
		java.awt.image.BufferedImage img2 = (java.awt.image.BufferedImage) img;
		javax.imageio.ImageIO.write(img2, "png", new java.io.File("./ville.png"));
	}
	
	static java.awt.Image createSampleImage(int size) {
		// Crée une nouvelle instance BufferedImage (subclasse d'Image)
		java.awt.image.BufferedImage img = new java.awt.image.BufferedImage(size, size, java.awt.image.BufferedImage.TYPE_INT_ARGB);
		
		// Fait un dessin aléatoire
		paintOnImage(img);
		
		return img;
	}
	
	// Radian ¿?
	public static double angleBetweenTwoPointsWith3rdPoint(double point1X, double point1Y, 
		double point2X, double point2Y, 
		double point3X, double point3Y) {

		double angle1 = Math.atan2(point1Y - point3Y, point1X - point3X);
		double angle2 = Math.atan2(point2Y - point3Y, point2X - point3X);

		return angle1 - angle2; 
	}

	static void paintOnImage(java.awt.image.BufferedImage img) {
		// Crée un Graphics2D dessinable (subclasse de Graphics)
		java.awt.Graphics2D g2d = (java.awt.Graphics2D) img.getGraphics();
		
		// Couleur du fond
		g2d.setColor(java.awt.Color.BLACK);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
		
		// Couleur des routes
		g2d.setColor(java.awt.Color.WHITE);
		
		// Paramètres pour le dessin aléatoire
		int nbChemins = 11;
		int tailleMinChemin = 80;
		int anglemax = 120;
		int anglemin = 40;

		double [] tabx = new double[nbChemins];
		double [] taby = new double[nbChemins];
		double tmpx = 0,tmpy=0,tmpangle=0;
		
		// Processus de dessin
		for(int i= 0; i < nbChemins; i++) { 
			if(i == 0) {
				tabx[0]=java.lang.Math.random() * img.getWidth();
				taby[0]=java.lang.Math.random() * img.getHeight();
			} else {			 
				do {
					tmpx = java.lang.Math.random() * img.getWidth();
					tmpy = java.lang.Math.random() * img.getHeight();
					if(i >=2) {
						tmpangle = Math.toDegrees(angleBetweenTwoPointsWith3rdPoint(tabx[i-2], taby[i-2],tmpx,tmpy, tabx[i-1], taby[i-1]));  
					}
				}while(
					/*Contrainte longueur route*/
					java.lang.Math.sqrt(
						java.lang.Math.pow(tmpx-tabx[i-1], 2) -
						java.lang.Math.pow(tmpy-taby[i-1], 2)
						)
					>=tailleMinChemin
					
					||

					/*Contrainte Angle route*/
					(   
						i >=2 
						&&
						(Math.abs(tmpangle) > anglemax || Math.abs(tmpangle) < anglemin )
					)
				);
				
				tabx[i] = tmpx;
				taby[i] = tmpy;
			}

		}
		
		// Changement de type
		int [] tabxInt = new int[nbChemins];
		int [] tabyInt = new int[nbChemins];
		
		for(int i= 0; i < nbChemins; i++) {
			tabxInt[i] = (int) tabx[i];
			tabyInt[i] = (int) taby[i];
		}
		
		g2d.drawPolyline(tabxInt, tabyInt, nbChemins);
		
		g2d.dispose();
	}
}
