import java.io.IOException;

/*class ImageCreation300x300 {
  
	static java.awt.Image createSampleImage() {
		java.awt.image.BufferedImage img = new java.awt.image.BufferedImage(300, 300, java.awt.image.BufferedImage.TYPE_INT_ARGB);
		
		paintOnImage(img);
		
		return img;
	}
	//Radian
	public static double angleBetweenTwoPointsWith3rdPoint(double point1X, double point1Y, 
		double point2X, double point2Y, 
		double point3X, double point3Y) {

		double angle1 = Math.atan2(point1Y - point3Y, point1X - point3X);
		double angle2 = Math.atan2(point2Y - point3Y, point2X - point3X);

		return angle1 - angle2; 
	}

	static void paintOnImage(java.awt.image.BufferedImage img) {

		java.awt.Graphics2D g2d = (java.awt.Graphics2D) img.getGraphics();
		
		// Couleur du fond
		g2d.setColor(java.awt.Color.BLACK);
		g2d.fillRect(0, 0, 300, 300);
		//couleur des routes
		g2d.setColor(java.awt.Color.WHITE);
		int nbChemins = 11;
		int tailleMinChemin = 80;
		int anglemax = 120;
		int anglemin = 40;
		double [] tabx = new double[nbChemins];
		double [] taby = new double[nbChemins];
		double tmpx = 0,tmpy=0,tmpangle=0;
		for(int i= 0; i<nbChemins;i++) { 
			if(i==0) {
				tabx[0]=java.lang.Math.random() * 300 ;
				taby[0]=java.lang.Math.random() * 300 ;
			} else {			 
				do {
					tmpx = java.lang.Math.random() * 300;
					tmpy = java.lang.Math.random() * 300;
					if(i >=2) {
						tmpangle = Math.toDegrees(angleBetweenTwoPointsWith3rdPoint(tabx[i-2], taby[i-2],tmpx,tmpy, tabx[i-1], taby[i-1]));  
					}
				}while(
					/*Contrainte longueur route*/
					/*java.lang.Math.sqrt(
						java.lang.Math.pow(tmpx-tabx[i-1], 2) -
						java.lang.Math.pow(tmpy-taby[i-1], 2)
						)
					>=tailleMinChemin
					
					||

					/*Contrainte Angle route*/
					/*(   
						i >=2 
						&&
						(Math.abs(tmpangle) > anglemax || Math.abs(tmpangle) < anglemin )
					)
				);
				
				tabx[i] = tmpx;
				taby[i] = tmpy;
			}*/
//import java.io.IOException;

class ImageCreation300x300 {
	  	
	public static void main(String[] args) throws IOException {
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		java.awt.Image img = createSampleImage();
		javax.swing.ImageIcon icon = new javax.swing.ImageIcon(img);
		frame.add(new javax.swing.JLabel(icon));
		frame.pack();
		frame.setVisible(true);
		java.awt.image.BufferedImage img2 = (java.awt.image.BufferedImage) img;
		javax.imageio.ImageIO.write(img2, "png", new java.io.File("./ville.png"));
	}
	
	static java.awt.Image createSampleImage(int size) {
		// instantiate a new BufferedImage (subclass of Image) instance 
		java.awt.image.BufferedImage img = new java.awt.image.BufferedImage(size, size, java.awt.image.BufferedImage.TYPE_INT_ARGB);
		
		//draw something on the image
		paintOnImage(img);
		
		return img;
	}
	
	//Radian
	public static double angleBetweenTwoPointsWith3rdPoint(double point1X, double point1Y, 
		double point2X, double point2Y, 
		double point3X, double point3Y) {

		double angle1 = Math.atan2(point1Y - point3Y, point1X - point3X);
		double angle2 = Math.atan2(point2Y - point3Y, point2X - point3X);

		return angle1 - angle2; 
	}

	static void paintOnImage(java.awt.image.BufferedImage img) {
		// get a drawable Graphics2D (subclass of Graphics) object 
		java.awt.Graphics2D g2d = (java.awt.Graphics2D) img.getGraphics();
		
		// Couleur du fond
		g2d.setColor(java.awt.Color.BLACK);
		g2d.fillRect(0, 0, 300, 300);
		//couleur des routes
		g2d.setColor(java.awt.Color.WHITE);
		int nbChemins = 11;
		int tailleMinChemin = 80;
		int anglemax = 120;
		int anglemin = 40;
		double [] tabx = new double[nbChemins];
		double [] taby = new double[nbChemins];
		double tmpx = 0,tmpy=0,tmpangle=0;
		for(int i= 0; i<nbChemins;i++) { 
			if(i==0) {
				tabx[0]=java.lang.Math.random() * 300 ;
				taby[0]=java.lang.Math.random() * 300 ;
			} else {			 
				do {
					tmpx = java.lang.Math.random() * 300;
					tmpy = java.lang.Math.random() * 300;
					if(i >=2) {
						tmpangle = Math.toDegrees(angleBetweenTwoPointsWith3rdPoint(tabx[i-2], taby[i-2],tmpx,tmpy, tabx[i-1], taby[i-1]));  
					}
				}while(
					/*Contrainte longueur route*/
					java.lang.Math.sqrt(
						java.lang.Math.pow(tmpx-tabx[i-1], 2) -
						java.lang.Math.pow(tmpy-taby[i-1], 2)
						)
					>=tailleMinChemin
					
					||

					/*Contrainte Angle route*/
					(   
						i >=2 
						&&
						(Math.abs(tmpangle) > anglemax || Math.abs(tmpangle) < anglemin )
					)
				);
				
				tabx[i] = tmpx;
				taby[i] = tmpy;
			}

		}
		//Changement de type
		int [] tabxInt = new int[nbChemins];
		int [] tabyInt = new int[nbChemins];
		for(int i= 0; i<nbChemins;i++) {
			tabxInt[i] = (int) tabx[i];
			tabyInt[i] = (int) taby[i];
			// if(i>=2) {

			//	 System.out.println(""+Math.toDegrees(angleBetweenTwoPointsWith3rdPoint(tabx[i-2], taby[i-2],tabx[i],taby[i], tabx[i-1], taby[i-1])));
			// }
		}
		g2d.drawPolyline(tabxInt, tabyInt, nbChemins);

		//g2d.setColor(java.awt.Color.RED);
		//g2d.drawRect(150, 70, 340, 340);
		
		// drawing on images can be very memory-consuming
		// so it's better to free resources early
		// it's not necessary, though
		g2d.dispose();
	}

/*;
			// if(i>=2) {

			//	 System.out.println(""+Math.toDegrees(angleBetweenTwoPointsWith3rdPoint(tabx[i-2], taby[i-2],tabx[i],taby[i], tabx[i-1], taby[i-1])));
			// }
		}
		g2d.drawPolyline(tabxInt, tabyInt, nbChemins);

	    //g2d.setColor(java.awt.Color.RED);
        //g2d.drawRect(150, 70, 340, 340);
        
        // drawing on images can be very memory-consuming
        // so it's better to free resources early
        // it's not necessary, though
        g2d.dispose();
    }

    public static void main(String[] args) throws IOException {
        javax.swing.JFrame frame = new javax.swing.JFrame();
        frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        java.awt.Image img = createSampleImage();
        javax.swing.ImageIcon icon = new javax.swing.ImageIcon(img);
        frame.add(new javax.swing.JLabel(icon));
        frame.pack();
        frame.setVisible(true);
        java.awt.image.BufferedImage img2 = (java.awt.image.BufferedImage) img;
        javax.imageio.ImageIO.write(img2, "png", new java.io.File("./ville.png"));
    }*/
}
